<?php

namespace Drupal\d01_drupal_suggestions;

/**
 * Class SuggestionService.
 *
 * @package Drupal\d01_drupal_suggestions
 */
class SuggestionService {

  /**
   * Constructs SuggestionService.
   */
  public function __construct() {}

  /**
   * Format a string for a valid suggestion.
   *
   * @param string $string
   *   An string to format to use azs suggestion.
   *
   * @return mixed
   *   A formatted string to use as suggestion.
   */
  public function formatForSuggestion($string) {
    $lower = strtolower($string);
    return preg_replace('/[\s-]+/', '_', $lower);
  }

  /**
   * Add item to theme suggestion array.
   *
   * @param array $suggestions
   *   List of theme suggestions.
   * @param string $suggestion
   *   Item to add to suggestions.
   *
   * @return array
   *   Updated list of suggestions.
   */
  public function addToSuggestionList(array $suggestions, $suggestion) {
    if (!in_array($suggestion, $suggestions)) {
      $suggestions[] = $suggestion;
    }
    return $suggestions;
  }

}
